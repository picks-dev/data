# PickSuite/Data

A library for modeling sports-betting data.

# Library Structure

#### Core

These are some custom Eloquent models of the fundamental data structures.

#### Builders

These are some custom Eloquent query builders, simplifying or correcting querying of the models.

#### Models

These are models of non-fundamental or derived meta-data structures.
