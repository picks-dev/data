<?php

namespace PickSuite\Data\Models;

use Illuminate\Support\Collection;
use PickSuite\Data\Core\Game;
use PickSuite\Data\Core\Pick;
use PickSuite\Data\Core\Score;
use function array_combine;

class SpreadsheetPicks extends Collection
{
    const HEADERS = [
        'date',
        'day',
        'aw team',
        'ho team',
        'away m/l',
        'home m/l',
        'o/u',
        'aw 1st',
        'ho 1st',
        'aw 1st5',
        'ho 1st5',
        'aw runs',
        'ho runs',
        'aw pitcher',
        'ho pitcher',
        'start time',
        'home plate ump',
        'away on',
        'away against',
        'away over',
        'away under',
        'home on',
        'home against',
        'home over',
        'home under',
    ];

    public static function fromGame(Game $game)
    {
        $counts = $game
            ->picks
            ->groupBy(function (Pick $pick) use ($game) {
                $foo = $pick->team->is($game->away) ? 'away' : 'home';
                $bar = $pick->isTotal
                    ? ($pick->total_over ? 'over' : 'under')
                    : ($pick->team->is($pick->side) ? 'on' : 'against');

                return $foo . ucfirst($bar);
            })
            ->map(function (Collection $picks) {
                return $picks->count();
            });

        return parent::make(array_combine(static::HEADERS, [
            $game->starts_at->format('ymd'),
            $game->starts_at->format('D'),
            $game->away->abbr,
            $game->home->abbr,
            $game->away_money_line,
            $game->home_money_line,
            $game->over_under,
            $game->scores->take(1)->sum(function (Score $score) {
                return $score->away_score;
            }),
            $game->scores->take(1)->sum(function (Score $score) {
                return $score->home_score;
            }),
            $game->scores->take(5)->sum(function (Score $score) {
                return $score->away_score;
            }),
            $game->scores->take(5)->sum(function (Score $score) {
                return $score->home_score;
            }),
            $game->scores->sum(function (Score $score) {
                return $score->away_score;
            }),
            $game->scores->sum(function (Score $score) {
                return $score->home_score;
            }),
            null,
            null,
            $game->starts_at->format('H'),
            null,
            $counts['awayOn'] ?? 0,
            $counts['awayAgainst'] ?? 0,
            $counts['awayOver'] ?? 0,
            $counts['awayUnder'] ?? 0,
            $counts['homeOn'] ?? 0,
            $counts['homeAgainst'] ?? 0,
            $counts['homeOver'] ?? 0,
            $counts['homeUnder'] ?? 0,
        ]));
    }
}
