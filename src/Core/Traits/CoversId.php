<?php

namespace PickSuite\Data\Core\Traits;

/**
 * @property string $covers_id
 */
trait CoversId
{
    protected function getUuidAttribute(): string
    {
        return $this->covers_id;
    }
}
