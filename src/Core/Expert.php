<?php

namespace PickSuite\Data\Core;

use PickSuite\Data\Core\Traits\CoversId;
use PickSuite\Data\Core\Traits\ScrapedAt;

class Expert extends Model
{
    use CoversId, ScrapedAt;

    const TABLE = 'experts';

    const ATTR_COVERS_ID = 'covers_id';
}
