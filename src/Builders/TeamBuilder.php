<?php

namespace PickSuite\Data\Builders;

use PickSuite\Data\Core\Game;

class TeamBuilder extends Builder
{
    public function whereSport(string $sportId)
    {
        return $this->whereHas(Game::RELATION_SPORT, function (Builder $builder) use ($sportId) {
            return $builder->whereId($sportId);
        });
    }
}
